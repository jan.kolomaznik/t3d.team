Python pro Tescan3dim
=============================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Náplň kurzu:
------------

- **[Základy programování v Python](00-python.ipynb)**
    - Tak prvně proč Python
    - Charakteristika
    - K čemu se python hodí?
    - Kdo python používá?
    - Instalace
    - Možnosti vývoje
    - Příprava na vývoj
    - IDE
- **Pokročilé datové typy**
    - [Čísla](01-python.type.numbers.ipynb)
    - [Řetězce: Formátování](02-python.type.str.ipynb)
    - [Regulární výrazy](03-python.type.regex.ipynb)
- **[Základní konstrukce jazyka](04-python.construction.ipynb)**
    - Podmínka (`if`)
    - Smyčka (`while`)
    - Smyčka (`for`)
    - [Ternární operátor](05-python.construction.if.ipynb)
    - [For při generování seznamů](06-python.construction.for.ipynb)
- **Funkce**
    - [Funkce](07-python.function.ipynb)
    - [Lambdas](08-python.function.lambdas.ipynb)
    - [Dekorátory](09-python.function.decorator.ipynb)
    - [Functools](10-python.function.functools.ipynb)
    - [nerátory](11-python.function.genarators.ipynb)
    - [Itertools](12-python.function.itertools.ipynb)
- **[Moduly](13-python.module.ipynb)**
    - Slovníček pojmů
    - Struktura modulu
- **[Chyby a výjimky](14-python.except.ipynb)**
    - Zachycení a ošetření výjimek
    - Kontext výjimek
    - Vyvolání výjimky
- **Persistence**
    - [Práce se soubory](15-python.file.ipynb)
    - [Ukládání a zpracování dat](16-python.save.ipynb)
- **[Třídy a objekty](17-python.object.ipynb)**
    - [Magické metody](18-python.object.magic.ipynb)
    - [First-Class Objects](19-python.object.first-class.ipynb)
    - [Deskriptory](20-python.object.descriptor.ipynb)
    - [Konstruktor](21-python.object.constuctor.ipynb)
    - [Metatřídy](22-python.object.metaclass.ipynb)
    * Vícenásobná dědičnost
- [**Ladění a logování**](23-python.logging.ipynb)
    1. Ladění pomocí výpisů
    2. Standardní logovací knihovna
    3. Debuggery
    4. Používání assertů ve vlastním kódu
- [**Psaní bezpečného kódu**](24-python.save.code.ipynb)
    1. Porovnávání hodnot
    2. Typová kontrola
    3. Okrajové případy
- [**Paralelní zpracování**](25-python.parallelization.ipynb)
    - [Práce s vlákna](26-python.parallelization.multihreading.ipynb)
    - [Práce s procesy](27-python.parallelization.multiprocessing.ipynb)    
- **[Testování](28-test.ipynb)**
    - [Testování v python](29-python.test.ipynb)
    - [Testovací framework (pytest)](30-test.pytest.ipynb)
    - [Testování kódu](31-test.code.ipynb)
    - [Psaní testovatelného kódu**](32-test.principles.ipynb)
    - [Negativní testování**](33-test.exceptions.ipynb)