# %% imports
from world import world as WORLD
from world import World
from collections import namedtuple
from itertools import count

# %% Create Package
Package = namedtuple("Package", ['location', 'destination'])

packages = [
    Package(WORLD[0].code, WORLD[1].code),
    Package(WORLD[0].code, WORLD[-2].code),
    Package(WORLD[1].code, WORLD[-1].code),
    Package(WORLD[3].code, WORLD[-2].code),
 ]
packages

# %%
class State:

    COUNTER = count()

    def __init__(self, robot:str, packages:list[Package]):
        self.__id = next(State.COUNTER)
        self.__robot = robot
        self.__packages = tuple(packages)

    def __repr__(self):
        return f"State {self.__id} [robot: {self.__robot}, packages: {len(self.__packages)}]"
    
    def move(self, position:str,*,world:World=WORLD) -> 'State':
        """Pohne robotem na pozici, kdyz je to mozne.

        Args:
            position (str): Code of country
            world (World, optional): _description_. Defaults to WORLD.

        Returns:
            State: _description_
        """

        
        if position not in world[self.__robot].borders:
            return self
        
        #packages = [
        #    Package(position, p.destination) if p.location == self.__robot else p
        #    for p in self.__packages
        #    if p.location != self.__robot or p.destination != position
        #]
        #
        #assert len(self.__packages) >= len(packages), f"Packege was created"
        #assert not any(map(lambda p: p.location == p.destination, packages)), f"At least one package is delivery and stay in the list."

        it = map(lambda p: Package(position, p.destination) if p.location == self.__robot else p, 
                 self.__packages)
        it = filter(lambda p: p.location != p.destination, 
                    it)

        return State(position, it)
        


begin = State(WORLD[0].code, packages)
begin
# %%
begin.move('POL')


# %%
