# %% imports
import requests
from collections import namedtuple

# %% define tuples
Country = namedtuple("Country", ['name', 'code', 'borders'])

Country('Czech', 'CZ', ['SK', 'DE'])

# %% dewince class World
class World:

    @staticmethod
    def from_restcountries(url):
        """This static method creates a World object using country data retrieved from the specified endpoint. The endpoint should point to an API like restcountries.com. 

        Args:
            url (str): The API endpoint from which country data will be retrieved.
                
        Returns:
            World: A World object containing the gathered country data.

        Raises:
            ValueError: If the endpoint is not a string or is an empty string.
            ConnectionError: If the connection to the API endpoint fails.
        """
        response = requests.get(url)
        if response.status_code != 200:
            raise ValueError(f"Respose is {response}")
        
        countries = [
            Country(
                name = c["name"]["common"],
                code = c["cca3"],
                borders = c["borders"] if "borders" in c else []
            ) for c in response.json()]
        return World(countries)
            

    def __init__(self, countries):
        if not isinstance(countries, list):
            raise ValueError(f"countries is not list, but {countries!r}")
        if not all(map(lambda i: isinstance(i, Country), countries)):
            raise ValueError(f"countries do not contins only Country.")
        
        self.__countries = countries

    def __repr__(self):
        return f"World {[c.name for c in self.__countries]}"
    
    def __str__(self):
        result = "World countries:\n"
        for c in self:
            result += f"\t{c.code} {'(' + c.name + ')':<10}: {c.borders}\n"
        return result

    def __getitem__(self, key):
        match key:
            case int(i):
                return self.__countries[i]
            case str(code):
                for c in self.__countries:
                    if c.code == code:
                        return c
            #case slice(start, stop, step):
            #    return start, stop, step
            
        raise IndexError(f"{key} is not valid index")

world = World.from_restcountries("https://restcountries.com/v3.1/region/Central Europe")
# %% Test load
world

# %%
print(world)

# %%
if __name__ == "__main__":
    print(world[-1])
    print(world['CZE'])

# %%
#world['CZE':'HUN']

