import pytest
from time import sleep


@pytest.mark.webtest
def test_send_http():
    sleep(4)  # perform some webtest test for your app


def test_something_quick():
    sleep(0.1)


@pytest.mark.another
def test_another():
    sleep(0.2)


class TestClass:
    def test_method(self):
        sleep(0.4)
